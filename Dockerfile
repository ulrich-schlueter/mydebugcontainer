FROM alpine:latest
RUN apk --update add redis
ENTRYPOINT ["/bin/ash", "-c", "sleep 100000000"]