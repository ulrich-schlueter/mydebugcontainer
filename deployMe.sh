#!/bin/bash
set -x
VERSION=$1
IMAGENAME=ulidebug

sed "s/XX_VERSION_XX/$VERSION/g" debug-deployment.yaml.template > debug-deployment.yaml

for name in debug-deployment.yaml
do
    echo $name
    oc delete -f $name
    oc apply  -f $name
done






